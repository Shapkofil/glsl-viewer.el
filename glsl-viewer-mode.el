;;; glsl-viewer-mode.el --- a simple package                   
(defcustom glsl-viewer-shader-file buffer-file-name
  "The file which contains the shader."
  :type 'string)

(defcustom glsl-viewer-keymap-prefix "C-c r"
  "Leader key for the keymap"
  :type 'string)

(defcustom glsl-viewer-position-override t
  "Leader key for the keymap"
  :type 'string)

(defun glsl-viewer-split-windows (w h)
  "Splits windows:
- glslViewer compilation
- glslViewer place holder (with pixel resolution w x h)
returns the pixel coordinates of the glslViewer placeholder"
  ;; multiply by -1 to move the split to the right
  (set-window-buffer (or (get-buffer-window "*glslViewer*") 
		         (split-window (get-buffer-window) (* w -1) t t))
		     (or (get-buffer "*glslViewer*")
			 (generate-new-buffer "*glslViewer*"))
		     )
  (set-window-buffer (or (get-buffer-window "*glslViewer-compilation*")
		         (split-window (next-window) h nil t))

		     (or (get-buffer "*glslViewer-compilation*")
			 (generate-new-buffer "*glslViewer-compilation*"))
		     )
  (window-absolute-pixel-edges (get-buffer-window "*glslViewer*"))
 ) 

(defun glsl-viewer-run (cmd)
  "Runs an instance of glslViewer inside emacs."
  (interactive
   (list
    (let
	((default-cmd (concat "glslViewer " buffer-file-name)))
      (read-string "Command: " default-cmd nil default-cmd)
      )))

  ;; Split compilation and glslViewer window
  (setq glsl-viewer-edges (glsl-viewer-split-windows 640 480))
  (and glsl-viewer-position-override
       (setq cmd (concat cmd (format " -x %d -y %d -w %d -h %d "
				     (elt glsl-viewer-edges 0)
				     (elt glsl-viewer-edges 1)
				     (- (elt glsl-viewer-edges 2) (elt glsl-viewer-edges 0))
				     (- (elt glsl-viewer-edges 3) (elt glsl-viewer-edges 1))
				     ))))

  ;; Using compilation buffer name function 
  (let
      (( compilation-buffer-name-function '(lambda (maj-mode)
					    "*glslViewer-compilation*" 
					    )))
  (compile cmd))

)

(defun glsl-viewer--key (key)
  (kbd (concat glsl-viewer-keymap-prefix " " key)))

(define-minor-mode glsl-viewer-mode
  "glslView wrapper as a minor mode"
  nil
  :global t
  :group 'shaders
  :lighter " glslViewer"
  :keymap
  (list (cons (glsl-viewer--key "r") #'glsl-viewer-run))
)

(provide 'glsl-viewer-mode)
;;; glsl-viewer-mode.el ends here
