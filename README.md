# glsl-viewer-mode.el

[glslViewer](https://github.com/patriciogonzalezvivo/glslViewer) wrapper as a minor mode in emacs

Works well with [glsl-mode](https://github.com/jimhourihan/glsl-mode) 

## Screenshot

![showcase](https://gitlab.com/Shapkofil/glsl-viewer.el/-/raw/master/sample.png)

## Installation

The project is a work in progress and isn't yet on MELPA

This is the current set up in my [personal dotfiles](https://gitlab.com/Shapkofil/garbagemacs/-/blob/master/Emacs.org)

```
(use-package glsl-mode)

(load-file "path to glsl-viewer-mode.el")

;; Auto start glsl viewer mode automatically when in a .frag file
(add-hook 'glsl-mode-hook (glsl-viewer-mode 1)) 

```

